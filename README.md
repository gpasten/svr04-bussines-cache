# SVR04-BUSSINES-CACHE
Este arquetipo fue desarrollado para banorte y apunta a ejemplificar cómo debe ser desarrollado un servicio que Agregara cache a la herramienta dataGrid.

Adicionalmente este arquetipo demuestra la incorporación de parametros y librerias necesarias para llevar acabo la conexion con esta herramienta.

# Contenido

- [x] Endpoints Rest
- [x] Creacion de cache
- [x] Envio de cache
- [x] Recibir y producir JSON
- [x] Despliegue en ocp / Comandos OC
- [x] Carpeta Openshift
- [x] Parametrizacion y Configuracion
- [x] Configuracion de Logs 
- [x] Manejo de excepciones
- [x] Junit Test

# Tecnologías utilizadas
- Quarkus: 3.2

- Java: 17

# Configuración
La configuración de la aplicación se encuentra en el archivo application.properties en la carpeta resources. Aquí se pueden ajustar diversos aspectos, como la configuración de la base de datos, los logs y otras propiedades específicas de Quarkus.

# Conexion con Data Gid
Configuracion para la conexion son RedHat Singles Sign On (RH SSO) mediante Open id Conect 

quarkus.infinispan-client.hosts -> Esta configuración indica los hosts a los que el cliente Infinispan se conectará. 
quarkus.infinispan-client.username -> Esta configuración se refiere al nombre de usuario utilizado para la autenticación al conectarse al servidor Infinispan. 
quarkus.infinispan-client.password -> Aquí se establece la contraseña asociada al nombre de usuario para la autenticación al servidor Infinispan.

# Manejo de logs
La configuración de logs se encuentra en el archivo application.properties. Aquí se pueden ajustar los niveles de log para diferentes paquetes y clases, así como configurar la salida de los logs.

# Manejo de excepciones
La aplicación muestra ejemplos de cómo manejar excepciones utilizando Camel. Se pueden agregar manejadores de excepciones personalizados en las rutas para capturar y manejar excepciones específicas según sea necesario.

# Pruebas JUnit 
La aplicacion muestra ejemplos de como testear metodos y funcionalidades con Junit5 y rest-assured.

# Carpeta OpenShift
La carpeta openshift contiene el archivo route-config.yaml, que proporciona configuraciones de rutas para OpenShift. Estas configuraciones se pueden utilizar para exponer los puntos de acceso REST de la aplicación en un clúster de OpenShift.

# Comandos OC 
Estos comandos sirven para desplegar el arquetipo en openshift desde la consola del bastion.

- oc new-build --binary=true --name=dg --image-stream=java:latest
- oc set env bc/dg OPENSHIFT_DG_HOSTS="" OPENSHIFT_DG_USERNAME="" OPENSHIFT_DG_PASSWORD=""
- oc start-build dg --from-dir=. --follow
- oc new-app dg
- oc expose service/dg

# Comandos
A continuación se presentan algunos comandos útiles para utilizar la aplicación:

#### Ejecutar la aplicación en modo de desarrollo:

mvn quarkus:dev
Ejecutar el proyecto en modo desarrollo
./mvnw compile quarkus:dev
Empaquetar y ejecutar la aplicación:

./mvnw package  
java -jar target/quarkus-app/quarkus-run.jar
Crear un ejecutable nativo:

./mvnw package -Pnative
Ejecutar el ejecutable nativo en un contenedor:

./mvnw package -Pnative -Dquarkus.native.container-build=true

